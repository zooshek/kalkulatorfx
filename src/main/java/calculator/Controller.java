package calculator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Controller {

    private static final Logger log = LoggerFactory.getLogger(Controller.class);

    public String liczba = "0";
    public String liczbaPrev = "";
    public String liczba1 = "0";
    public String liczba2 = "0";
    public String znak = "";
    public String znakprev = "";
    public double liczbaDouble ;
    public double liczbaDouble1 = 0;
    public double liczbaDouble2 = 0;
    public double wynikDouble = 0;

    @FXML
    private TextField textTest;
    @FXML
    private TextField textFieldPrev;
    @FXML
    private TextField textField;
    @FXML
    private Button button0;
    @FXML
    private Button button1;
    @FXML
    private Button button2;
    @FXML
    private Button button3;
    @FXML
    private Button button4;
    @FXML
    private Button button5;
    @FXML
    private Button button6;
    @FXML
    private Button button7;
    @FXML
    private Button button8;
    @FXML
    private Button button9;
    @FXML
    private Button buttonPlusMinus;
    @FXML
    private Button buttonIs;
    @FXML
    private Button buttonComma;
    @FXML
    private Button buttonPlus;
    @FXML
    private Button buttonMinus;
    @FXML
    private Button buttonX;
    @FXML
    private Button buttonDivide;
    @FXML
    private Button buttonBack;
    @FXML
    private Button buttonC;
    @FXML
    private Button buttonCE;
    @FXML
    void clearAll(Button buttonClear)
    {
     if (buttonClear.textProperty().getValue()=="C");
        {
            liczba = "0";
            liczbaPrev = "";
            liczba1 = "0";
            liczba2 = "0";
            znak = "";
            znakprev = "";
            liczbaDouble =0;
            liczbaDouble1 = 0;
            liczbaDouble2 = 0;
            wynikDouble = 0;
            textField.setText("");
            textFieldPrev.setText("");
        }
    }
    void isEqual ()
    {
        znak = znakprev;
        if (znak.equals("=") ==false)
        {
            this.calculate();

            textFieldPrev.setText("");
            textField.setText(String.valueOf(liczbaDouble1));
            liczbaDouble = liczbaDouble1;
            liczbaDouble1 = 0;

            liczba = "";
            znakprev = "=";
            liczbaPrev = "";
        }
    }
    void displayCalc(Button buttonNumber)
    {

        if (liczba.equals("0"))
        {
            liczba = buttonNumber.getText();
        }
        else {
            liczba += buttonNumber.getText();
        }
        if (liczbaDouble1!=0) {
            textFieldPrev.setText(liczbaPrev);
        }
        textField.setText(liczba);
        liczbaDouble = Double.parseDouble(liczba.replace(",","."));
        znakprev = znak;
        znak ="";
    }

    void setSign(Button buttonSign) {
        if (znak.equals("")||znakprev.equals("=")||znakprev.equals("+-"))
        {
            znak = buttonSign.textProperty().getValue();
            znakprev = znak;

            this.calculate();

            textFieldPrev.setText(String.valueOf(liczbaPrev) + " " + znak);
            textField.setText(String.valueOf(liczbaDouble1));
            liczba = "";
        }
    }
    void calculate()
    {
        if (liczbaDouble1 == 0) {
            liczbaDouble1 = liczbaDouble;
            liczbaPrev = String.valueOf(liczbaDouble1);
            liczba1 = liczba;
            textField.setText(liczba);
            textFieldPrev.setText(String.valueOf(liczbaPrev) + " " + znak);
            liczbaDouble2 = 0;
            liczba = "";
        } else {
            liczbaDouble2 = liczbaDouble;
            liczba2 = liczba;
            if (znak.equals("+")) {
                liczbaPrev = liczbaPrev + " " + znak + " " + String.valueOf(liczbaDouble2);
                liczbaDouble1 = liczbaDouble1 + liczbaDouble2;
            } else if (znak.equals("-")) {
                liczbaPrev = liczbaPrev + " " + znak + " " + String.valueOf(liczbaDouble2);
                liczbaDouble1 = liczbaDouble1 - liczbaDouble2;
            } else if (znak.equals("*")) {
                liczbaPrev = liczbaPrev + " " + znak + " " + String.valueOf(liczbaDouble2);
                liczbaDouble1 = liczbaDouble1 * liczbaDouble2;
            } else if (znak.equals("/")) {
                liczbaPrev = liczbaPrev + " " + znak + " " + String.valueOf(liczbaDouble2);
                liczbaDouble1 = liczbaDouble1 / liczbaDouble2;
            }
            textFieldPrev.setText(String.valueOf(liczbaPrev) + " " + znak);
            textField.setText(String.valueOf(liczbaDouble1));
            liczba = "";

        }
    }
    void changeSign()
    {
        if (liczbaDouble != 0 )
        {

                liczbaDouble = liczbaDouble * -1;

        }
        textField.setText(String.valueOf(liczbaDouble));
    }

    @FXML
     void runButtonAction(ActionEvent event) {
        log.info("Wciśnięto: " + event.getSource());
        log.debug("WWWW");
        log.error("ERROR");

        if (event.getSource() == buttonC) {
            this.clearAll(buttonC);
        } else if (event.getSource() == button1) {
            this.displayCalc(button1);
        } else if (event.getSource() == button2) {
            this.displayCalc(button2);
        } else if (event.getSource() == button3) {
            this.displayCalc(button3);
        } else if (event.getSource() == button4) {
            this.displayCalc(button4);
        } else if (event.getSource() == button5) {
            this.displayCalc(button5);
        } else if (event.getSource() == button6) {
            this.displayCalc(button6);
         } else if (event.getSource() == button7) {
            this.displayCalc(button7);
        } else if (event.getSource() == button8) {
            this.displayCalc(button8);
        } else if (event.getSource() == button9) {
            this.displayCalc(button9);
        } else if (event.getSource() == button0) {
            this.displayCalc(button0);
        } else if (event.getSource() == buttonPlus) {
            this.setSign(buttonPlus);
        } else if (event.getSource() == buttonMinus) {
            this.setSign(buttonMinus);
        } else if (event.getSource() == buttonX) {
            this.setSign(buttonX);
        } else if (event.getSource() == buttonDivide) {
            this.setSign(buttonDivide);
        } else if (event.getSource() == buttonIs) {
            this.isEqual();
        }else if (event.getSource() == buttonPlusMinus) {
            this.changeSign();
        } else if (event.getSource() == buttonComma) {
            if (liczba.contains(",") == false) {
                liczba += buttonComma.getText();
                textField.setText(liczba);
                liczbaDouble = Double.parseDouble(liczba.replace(",","."));
            }
        }
        else if (event.getSource() == buttonBack) {
            if (liczba.equals("0")==false )
            {
                if (liczba.length()>1) {
                    liczba = liczba.substring(0, liczba.length() - 1);
                } else liczba= "0";
            }
            textField.setText(liczba);
            liczbaDouble = Double.parseDouble(liczba.replace(",","."));
        }
    }
}
